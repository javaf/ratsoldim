unimul := proc (u::Vector[row])
  local listn, ilamx, ilamx1, myroots1, myroots, myroots2, mymin, ilamy, lam, degr, ilam, wmin, i, j, jv, k, l, n, multiplier, tt, listpol, size_list, m, mm, tt1;
  n := nops(u);
  listn := factors(u[n]);
  listpol := listn[2];
  size_list := nops(listpol); 
  multiplier := 1;
  ilamx1 := vector[row](size_list);
  for l from 1 to size_list do
       m := Vector[row](n);
       for j from 1 to n do
          m[j] := 0;
          tt := factors(u[j]);
          tt1 := tt[2];
          for k from 1 to nops(tt1) do
             if  (listpol[l][1]=tt1[k][1]) then
               m[j] := tt1[k][2];
             end if;
          end do;
       end do; 
       m := Vector[row](n, i -> m[i]-i+1);
       ilam := 0;
       wmin := min(m);
       for j from 1 to n do
         if (wmin=m[j]) then
           lam := 1; 
           for i from 1 to j-1 do 
             lam := lam*(y-i+1); 
           end do; 
           degr := m[j]+j-1;
           if (degr > 0) then
             ilam := ilam+lam*(diff(u[j], x$degr)); 
           else 
             ilam := ilam+lam*u[j];
           end if;
         end if;
       end do;
       ilamy := 0;
       jv := degree(ilam, y);
       for i from 1 to (jv + 1) do
         if (coeff(ilam, y, i-1) <> 0) then
           ilamy := ilamy + rem(listpol[l][1],coeff(ilam, y, i-1),x)*y^(i-1);
         end if;
       end do;
       jv := degree(ilamy, x);
       ilamx := Vector[row](jv+1);
       for i from 1 to jv+1 do
         ilamx[i] := coeff(ilamy, x, i-1);
       end do;
       ilamx1[l] := select[flatten](x -> 0 <> x, ilamx);
       jv := nops(ilamx1);
       myroots1 := map(e -> rhs(op(e)),[isolve(ilamx1[l][1])]);
       myroots := {}; 
       for i from 1 to nops(myroots1) do 
         myroots := myroots union {myroots1[i]}; 
       end do;
       if (jv > 1) then 
         for i from 2 to jv do
           myroots1 := map(e -> rhs(op(e)),[isolve(ilamx1[l][i])]);
           if (nops(myroots1) = 0) then
             return FAIL;
           end if;
           myroots2 := {};
           for j from 1 to nops(myroots1) do 
             myroots2 := myroots2 union {myroots1[j]}; 
           end do;
           myroots := myroots intersect myroots2;
         end do;
       end if;
       if (nops([myroots]) = 0) then
         return FAIL;
       end if;
       mymin := min(myroots);
       multiplier := multiplier * listpol[l][1]^mymin;
  end do;
  return multiplier;
end proc:

cyclic_vector := proc (A::Matrix) 
  local c, i, j, u, lam, B, w, den, BB, n, s;
  n := LinearAlgebra:-RowDimension(A);  
  c := LinearAlgebra:-RandomVector[row](n,generator=rand(n)+1);
  B := Matrix(n, n); 
  w := Vector[row](n+1); 
  den := 1;                                            
  BB := Matrix(n, n+1); 
  if n <> LinearAlgebra:-ColumnDimension(A) then 
    error "Matrix is not square" 
  end if; 
  for i from 1 to n do 
    B[i, 1..n] := c;
    c := map(diff, c, x) + c.A;  
  end do;  
  BB[1..n,1..n] := B; 
  BB[1..n, n+1] := c;                                       
  if LinearAlgebra:-Rank(BB)>LinearAlgebra:-Rank(B) then 
    c := LinearAlgebra:-RandomVector[row](n,generator=rand(n)+1);
    c := map(a -> a*x^(n-1), c);
    for i from 1 to n do 
      B[i, 1..n] := c;
      c := map(diff, c, x) + c.A;
    end do; 
  end if;                                                       
  u := LinearAlgebra:-LinearSolve(LinearAlgebra:-Transpose(B), convert(c, Vector[column]),free = 's');   
  s := 0;
  for i from 1 to n do 
    if (u[i] <> 0) then
      den := lcm(den,denom(u[i])); 
    end if;
  end do;
  w[n+1] := den; 
  w[1..n] := map(x -> simplify(-x*den), u);
  return w;
  end proc:

polSolDimOfScalarEquation := proc(u::Vector)
  local n, ilam, degmax, w, wmax, i, lam, j, solut, solut1, nelem, ob_sol, my_subst, e, my_matr, my_rank;
  n := LinearAlgebra:-Dimension(u) - 1;
  ilam := 0; 
  degmax := -1; 
  w := Vector(n+1, i->degree(u[i], x)-i+1);
  wmax := max(w,-n); 
  for i to n+1 do 
    if w[i] = wmax then 
      lam := 1; 
      for j to i-1 do 
        lam := lam*(x-j+1); 
      end do; 
      ilam := ilam+lam*lcoeff(u[i], x); 
    end if; 
  end do; 
  solut := isolve(ilam);
  solut1 := [];
  solut1 := map(e -> rhs(op(e)), [solut]); 
  solut1 := select[flatten](x -> 0 <= x, solut1);
  nelem := nops(solut1); 
  degmax := max(solut1, degmax);
  if (degmax < 0) then
    return 0;
  end if;
  if (degmax = 0) then
    ob_sol := z[0];
  else
    ob_sol := sum(z[k]*x^k, k = 0..degmax);
  end if;
  my_subst := 0;
  for i from 1 to n do
    my_subst := my_subst+u[i+1]*(diff(ob_sol, x$i));
  end do;
  my_subst := my_subst+u[1]*ob_sol;
  wmax := wmax+degmax;
  for i from 0 to wmax do
    e[i] := coeff(my_subst, x, i);
  end do;
  my_matr := Matrix(max(0, wmax), degmax+1);
  for i from 1 to wmax do
    for j from 1 to degmax + 1 do
      my_matr[i, j] := coeff(e[i], z[j-1], 1);
    end do;
  end do;
  my_rank := LinearAlgebra:-Rank(my_matr);
  return degmax+1-my_rank;
end proc:

ratSolDimOfScalarEquation := proc(u::Vector)
  local ud, n, den, i, u1;
  n := LinearAlgebra:-Dimension(u) - 1;
  ud := unimul(u);
  if (ud = FAIL) then 
    return 0;
  end if;
  u1 := Vector[row](n+1);
  den := 1;
  u1 := map(e -> e * ud, u); 
  for i from 1 to n+1 do 
    if (u1[i] <> 0) then
      den := lcm(den,denom(u1[i])); 
    end if;
  end do;
  u1[1..n+1] := map(x -> simplify(x*den), u1);
  return polSolDimOfScalarEquation(u1);
end proc:

ratSolDimOfSystem := proc (A::Matrix)
  local n, u;
  n := LinearAlgebra:-RowDimension(A);
  u := cyclic_vector(A);
  return ratSolDimOfScalarEquation(u);
end proc:

polSolDimOfSystem := proc (A::Matrix)
  local n, u;
  n := LinearAlgebra:-RowDimension(A);
  u := cyclic_vector(A);
  return polSolDimOfScalarEquation(u);
end proc:
